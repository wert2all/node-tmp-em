'use strict';
const Sequelize = require('sequelize');

const DBConnectionSequelizeAdapter = require('w-node-db-adapters').connection;
const DBRepository = require('w-node-db-repository');
const DBModelFactory = require('w-node-db-model').Factory;
const DBRepositoryOrderDefault = require('w-node-db-repository').OrderDefault;
const DBRepositoryLimitDefault = require('w-node-db-repository').LimitDefault;

const dbConnection = new DBConnectionSequelizeAdapter(
    Sequelize,
    'mysql://root:12345@mysql:3306/test'
);
const modelFactory = new DBModelFactory(dbConnection);

const Cron = require('./db/definitions').cron;

(async function () {
    /**
     *
     * @type {DBModelInterface}
     */
    const cronModel = await modelFactory.create(Cron);
    const repo = new DBRepository(cronModel);

    console.log(
        await repo
            .setOrders([new DBRepositoryOrderDefault('uuid', 'desc')])
            .setLimit(new DBRepositoryLimitDefault(1, 1))
            .find(['uuid'])
            .catch(e => console.log(e))
    );
}());

