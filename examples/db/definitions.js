'use strict';
const DBModelDefinitionBuilder = require('w-node-db-model-definition');
const DBRelationFactory = require('w-node-db-model-definition').relation;

const modelDefinitionBuilder = new DBModelDefinitionBuilder();
const modelRelationFactory = new DBRelationFactory();

const User = modelDefinitionBuilder
    .model()
    .addColumn(
        modelDefinitionBuilder.column()
            .setAllowed(true)
            .setType('string')
            .setSize(20)
            .build('name')
    )
    .build('user');

const Project = modelDefinitionBuilder
    .model()
    .addColumn(
        modelDefinitionBuilder.column()
            .setAllowed(true)
            .setType('string')
            .setSize(20)
            .build('name')
    )
    .setRelation(modelRelationFactory.many(User, 'children'))
    .build('project');


const Cron = modelDefinitionBuilder
    .model()
    .addColumn(
        modelDefinitionBuilder
            .column()
            .setAllowed(false)
            .setType('string')
            .setSize(20)
            .setDefault('')
            .build('uuid')
    )
    .addColumn(
        modelDefinitionBuilder
            .column()
            .setAllowed(false)
            .setType('string')
            .setSize(20)
            .setDefault('process')
            .build('status')
    )
    .addColumn(
        modelDefinitionBuilder
            .column()
            .setAllowed(true)
            .setType('string')
            .setSize(200)
            .build('message')
    )
    .build('cron');

module.exports = {
    'cron': Cron,
    'user': User,
    'project': Project
};
