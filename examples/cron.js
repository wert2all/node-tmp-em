'use strict';

const CM = require('w-node-cron');
const CronComposite = require('w-node-cron').Composite;
const CronItemExample = require('w-node-cron').CronTaskExample;
const CronItemOption = require('w-node-cron').CronTaskOption;
const CronStatusDB = require('w-node-cron').CronStatusAdapters.DB;
const EMEntityManager = require('w-node-entity-manager');

const Sequelize = require('sequelize');
const DBConnectionSequelizeAdapter = require('w-node-db-adapters').connection;
const DBModelFactory = require('w-node-db-model').Factory;
const dbConnection = new DBConnectionSequelizeAdapter(
    Sequelize,
    'mysql://root:12345@mysql:3306/test'
);
const modelFactory = new DBModelFactory(dbConnection);
const entityManager = new EMEntityManager(dbConnection);

const Cron = require('./db/definitions').cron;

(async function () {
    /**
     *
     * @type {DBModelInterface}
     */
    const cronModel = await modelFactory.create(Cron);

    const cronManager = new CM(
        new CronComposite(
            [
                new CronItemExample(
                    new CronItemOption({
                        isActive: true
                    }),
                    'first'
                ),
            ]
        ),
        new CronStatusDB(cronModel, entityManager)
    );

    cronManager.run();
}());