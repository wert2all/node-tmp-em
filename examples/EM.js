'use strict';

const Sequelize = require('sequelize');

const DBConnectionSequelizeAdapter = require('w-node-db-adapters').connection;
const EMEntityManager = require('w-node-entity-manager');
const DBModelFactory = require('w-node-db-model').Factory;

const dbConnection = new DBConnectionSequelizeAdapter(
    Sequelize,
    'mysql://root:12345@mysql:3306/test'
);
const entityManager = new EMEntityManager(dbConnection);
const modelFactory = new DBModelFactory(dbConnection);

const User = require('./db/definitions').user;
const Project = require('./db/definitions').project;

(async function () {
    const projectModel = await modelFactory.create(Project);
    const userModel = await modelFactory.create(User);

    let project = entityManager.create(projectModel, {name: 'first project'});
    project = await entityManager.save(project);
    project = await entityManager.save(
        project.setValue('name', 'changed name')
            .addChild(entityManager.create(userModel, {name: 'first'}))
            .addChild(entityManager.create(userModel, {name: 'second'}))
    );

    console.log(project);
}());