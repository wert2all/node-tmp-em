#!/usr/bin/env bash

#rm -rf node_modules/
rm -rf yarn.lock

yarn config set registry http://verdaccio:4873
yarn install
yarn run start
